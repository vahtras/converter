#!/usr/bin/env python

import __future__

import sys

basname = sys.argv[1]

# ==> primitive class <===

class Primitive(object):

    def __init__(self, angl, e, c):
        self.angl = angl.upper()
        self.e = e
        self.c = c
        self.e_sci_notation = ("E" in e)
        self.c_sci_notation = ("E" in c)

    def e_string(self):
        if self.e_sci_notation:
            return "%18s"    % self.e
        else:
            shift = len(self.e)-15
            if (shift > 0 and shift <= 3):
                return "%*s%*s" % (15+shift, self.e, 3-shift, "")
            else:
                return "%15s   " % self.e

    def c_string(self):
        if self.c_sci_notation:
            c_digits = self.c.split("E")[0].split(".")[1]
            shift = len(c_digits)-6
            if (shift >= -3 and shift < 0):
                return "%*s%*s" % (23+shift, self.c, -shift, "")
            else:
                return "%23s"    % self.c
        else:
            c_digits = self.c.split(".")[1]
            shift = len(c_digits)-7
            if (shift > 0 and shift <= 3):
                return "%*s%*s" % (20+shift, self.c, 3-shift, "")
            else:
                return "%20s   " % self.c

# ==> compare primitive exponents between two AO shells <===

def check_same_exp(shell1, shell2):
    if len(shell1) != len(shell2):
        return False
    for prim1, prim2 in zip(shell1, shell2):
        if prim1.angl != prim2.angl:
            return False
        if prim1.e != prim2.e:
            return False
    return True

# ==> read EMSL basis set <==

f_emsl = open(basname + ".emsl.bs", "r")

all_element_basis = []
element_basis = []

for line in f_emsl:
    line = line.replace("D+", "E+")
    line = line.replace("D-", "E-")
    line = line.rstrip()
    if "****" in line:
        all_element_basis.append(element_basis)
        element_basis = []
    else:
        element_basis.append(line)

f_emsl.close()

# ==> write generally contracted basis set <==

f_gc = open(basname + ".gc.bs", "w")

if "pvdz" in basname:
    basname = basname.replace("pvdz", "pVDZ")
if "pvtz" in basname:
    basname = basname.replace("pvtz", "pVTZ")

f_gc.write("!\n")
f_gc.write("BASIS=\"" + basname + "\"\n")
f_gc.write("GENERAL\n")

title = all_element_basis.pop(0)

for ind in range(len(all_element_basis)):
    element_basis = all_element_basis[ind]

    # write element

    name = element_basis.pop(0)
    f_gc.write("%s\n" % name)

    # process AO shells and primitives

    shell_titles = []

    basis = []
    count = 0

    while count < len(element_basis):

        # AO shell, e.g. "S   3   1.00"
        shell_title = element_basis[count]
        shell_titles.append(shell_title)
        angl, nprim, tmp_one = shell_title.split()
        assert angl.upper() in ["S", "P", "D", "F", "G", "H"]
        assert float(tmp_one) == 1.0
        count += 1

        shell = []
        for iprim in range(int(nprim)):
            # exponent and coefficient
            e,c = element_basis[count].split()
            shell.append(Primitive(angl, e, c))
            count += 1

        basis.append(shell)

    # process general contractions

    gc_groups = []
    for i in range(len(basis)):
        gc_groups.append([i])

    for i in range(len(basis)):
        if len(gc_groups[i]) == 0:
            continue
        for j in range(i+1,len(basis)):
            if len(gc_groups[j]) == 0:
                continue
            same_exp = check_same_exp(basis[i], basis[j])
            if same_exp:
                gc_groups[i].append(j)
                gc_groups[j] = []

    # write generally contracted basis set

    for gc in gc_groups:
        if len(gc) == 0:
            continue

        # gc contains indices of AO shells with the same set of exponents
        # basis[gc[0]] is the first AO shell among them

        shell_title = shell_titles[gc[0]]
        for k in gc:
            assert shell_title == shell_titles[k]

        content = shell_title.split()
        assert(int(content[1]) == len(basis[gc[0]]))
        assert(float(content[2]) == 1.0)
        f_gc.write("%s%5d%5d\n" % (content[0], int(content[1]), len(gc)))

        shell_0 = basis[gc[0]]
        for i in range(len(shell_0)):
            f_gc.write(shell_0[i].e_string())
            for k in gc:
                shell_k = basis[k]
                f_gc.write(shell_k[i].c_string())
            f_gc.write("\n")

    # end of element basis set

    f_gc.write("****\n")

f_gc.close()
