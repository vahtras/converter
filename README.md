# Converter

The purpose of this repository is to host some scripts for converting files
between different formats.

+ Truncate double-strand DNA

    Convert double-strand DNA (pdb format) to truncated base pairs (xyz format)

+ Convert general contraction

    Convert EMSL basis set format to generally contracted format

+ VeloxChem basis set

    Convert EMSL basis set format to VeloxChem format
